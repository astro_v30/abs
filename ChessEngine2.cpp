#include<bits/stdc++.h>

using namespace std;

#define ll long long int
#define fo(i,b,n) for(int i=(b); i<(n) ; i++)
#define xx first
#define yy second
#define pb push_back
#define sz(n) int(n.size())
#define pii pair
#define MP make_pair

#define read(x) scanf("%d",&x)
#define read2(x,y) scanf("%d%d",&x,&y)
#define readl(x) scanf("%lld",&x)
#define readl2(x,y) scanf("%lld%lld",&x,&y)
#define readd(x) scanf("%lf",&x)
#define readd2(x,y) scanf("%lf%lf",&x,&y)

#define pr(n) printf("%d",n)
#define prn(n) printf("%d\n",n)
#define prl(n) printf("%lld",n)
#define prln(n) printf("%lld\n",n)
#define prd(x) printf("%lf",x)
#define prdn(x) printf("%lf\n",x)

#define TC(n) printf("Case %d: ",n)

char board[10][10];
int kx[]= {2,1,-1,-2,-2,-1,1,2};
int ky[]= {1,2,2,1,-1,-2,-2,-1};
int white_check_board[10][10],black_check_board[10][10];

int value(char ch)
{
    ch=tolower(ch);
    if(ch=='p')return 1;
    else if(ch=='b' || ch=='n')return 2;
    else if(ch=='r')return 3;
    else return 4;
}

struct node
{
    int from_x,from_y,to_x,to_y,p;
    char ch;
};


bool lim(int x,int y)
{
    return ( x>0 && x<5 && y>0 && y<5 );
}

bool LOWER(char ch)
{
    return (ch>='a' && ch<'z' );
}


void set_rook(int i,int j,int flag)
{

    if(flag)
    {

        int x,y;
        x=i,y=j;
        while( true )
        {
            x++;
            if( ! lim(x,y) ) break;
            if(board[x][y]=='.')
            {
                white_check_board[x][y]=1;
            }
            else
            {
                if( ! LOWER(board[x][y])) white_check_board[x][y]=1;
                break;
            }
        }

        x=i,y=j;
        while( true )
        {
            x--;
            if( ! lim(x,y) ) break;
            if(board[x][y]=='.')
            {
                white_check_board[x][y]=1;
            }
            else
            {
                if( ! LOWER(board[x][y])) white_check_board[x][y]=1;
                break;
            }
        }

        x=i,y=j;
        while( true )
        {
            y++;
            if( ! lim(x,y) ) break;
            if(board[x][y]=='.')
            {
                white_check_board[x][y]=1;
            }
            else
            {
                if( ! LOWER(board[x][y])) white_check_board[x][y]=1;
                break;
            }
        }

        x=i,y=j;
        while( true )
        {
            y--;
            if( ! lim(x,y) ) break;
            if(board[x][y]=='.')
            {
                white_check_board[x][y]=1;
            }
            else
            {
                if( ! LOWER(board[x][y])) white_check_board[x][y]=1;
                break;
            }
        }
    }
    else
    {
        int x,y;
        x=i,y=j;
        while( true )
        {
            x++;
            if( ! lim(x,y) ) break;
            if(board[x][y]=='.')
            {
                black_check_board[x][y]=1;
            }
            else
            {
                if(  LOWER(board[x][y])) black_check_board[x][y]=1;
                break;
            }
        }

        x=i,y=j;
        while( true )
        {
            x--;
            if( ! lim(x,y) ) break;
            if(board[x][y]=='.')
            {
                black_check_board[x][y]=1;
            }
            else
            {
                if(  LOWER(board[x][y])) black_check_board[x][y]=1;
                break;
            }
        }

        x=i,y=j;
        while( true )
        {
            y++;
            if( ! lim(x,y) ) break;
            if(board[x][y]=='.')
            {
                black_check_board[x][y]=1;
            }
            else
            {
                if(  LOWER(board[x][y])) black_check_board[x][y]=1;
                break;
            }
        }

        x=i,y=j;
        while( true )
        {
            y--;
            if( ! lim(x,y) ) break;
            if(board[x][y]=='.')
            {
                black_check_board[x][y]=1;
            }
            else
            {
                if(  LOWER(board[x][y])) black_check_board[x][y]=1;
                break;
            }
        }

    }
}

void set_bishop(int i,int j,int flag)
{
    if(flag)
    {

        int x,y;
        x=i,y=j;
        while( true )
        {
            x++,y++;
            if( ! lim(x,y) ) break;
            if(board[x][y]=='.')
            {
                white_check_board[x][y]=1;
            }
            //else break;
            else
            {
                if( ! LOWER(board[x][y])) white_check_board[x][y]=1;
                break;
            }
        }

        x=i,y=j;
        while( true )
        {
            x--,y--;
            if( ! lim(x,y) ) break;
            if(board[x][y]=='.')
            {
                white_check_board[x][y]=1;
            }
            //else break;
            else
            {
                if( ! LOWER(board[x][y])) white_check_board[x][y]=1;
                break;
            }
        }

        x=i,y=j;
        while( true )
        {
            x++,y--;
            if( ! lim(x,y) ) break;
            if(board[x][y]=='.')
            {
                white_check_board[x][y]=1;
            }
            //else break;
            else
            {
                if( ! LOWER(board[x][y])) white_check_board[x][y]=1;
                break;
            }
        }

        x=i,y=j;
        while( true )
        {
            x--,y++;
            if( ! lim(x,y) ) break;
            if(board[x][y]=='.')
            {
                white_check_board[x][y]=1;
            }
            //else break;
            else
            {
                if( ! LOWER(board[x][y])) white_check_board[x][y]=1;
                break;
            }
        }
    }
    else
    {
        int x,y;
        x=i,y=j;
        while( true )
        {
            x++,y++;
            if( ! lim(x,y) ) break;
            if(board[x][y]=='.')
            {
                black_check_board[x][y]=1;
            }
            //else break;
            else
            {
                if(  LOWER(board[x][y])) black_check_board[x][y]=1;
                break;
            }
        }

        x=i,y=j;
        while( true )
        {
            x--,y--;
            if( ! lim(x,y) ) break;
            if(board[x][y]=='.')
            {
                black_check_board[x][y]=1;
            }
            //else break;
            else
            {
                if(  LOWER(board[x][y])) black_check_board[x][y]=1;
                break;
            }
        }

        x=i,y=j;
        while( true )
        {
            x++,y--;
            if( ! lim(x,y) ) break;
            if(board[x][y]=='.')
            {
                black_check_board[x][y]=1;
            }
            //else break;
            else
            {
                if(  LOWER(board[x][y])) black_check_board[x][y]=1;
                break;
            }
        }

        x=i,y=j;
        while( true )
        {
            x--,y++;
            if( ! lim(x,y) ) break;
            if(board[x][y]=='.')
            {
                black_check_board[x][y]=1;
            }
            //else break;
            else
            {
                if(  LOWER(board[x][y])) black_check_board[x][y]=1;
                break;
            }
        }

    }
}

void set_knight(int i,int j,int flag)
{
    if(flag)
    {
        fo(c,0,8)
        {
            int x=i+kx[c],y=j+ky[c];
            if(lim(x,y) && ( board[x][y]=='.' || !LOWER(board[x][y] ) )  )
            {
                white_check_board[x][y]=1;
            }
        }
    }
    else
    {
        fo(c,0,8)
        {
            int x=i+kx[c],y=j+ky[c];
            if(lim(x,y) && ( board[x][y]=='.' || LOWER(board[x][y] ) )  )
            {
                black_check_board[x][y]=1;
            }
        }
    }
}

void set_white_check()
{
    memset(white_check_board,0,sizeof white_check_board);
    for(int i=1; i<5; i++)
        for(int j=1; j<5; j++)
        {
            if(board[i][j]=='P')
            {
                if(lim(i+1,j-1) && board[i+1][j-1]=='.' )
                    white_check_board[i+1][j-1]=1;
                if(lim(i+1,j+1) && board[i+1][j+1]=='.' )
                    white_check_board[i+1][j+1]=1;
            }
            if(board[i][j]=='R')
            {
                set_rook(i,j,1);
            }
            if(board[i][j]=='B')
            {
                set_bishop(i,j,1);
            }
            if(board[i][j]=='N')
            {
                set_knight(i,j,1);
            }
            if(board[i][j]=='Q')
            {
                set_rook(i,j,1);
                set_bishop(i,j,1);
            }
        }
}

void set_black_check()
{
    memset(black_check_board,0,sizeof black_check_board);
    for(int i=1; i<5; i++)
        for(int j=1; j<5; j++)
        {
            if(board[i][j]=='p')
            {
                if(lim(i+1,j-1) && board[i+1][j-1]=='.' )
                    black_check_board[i+1][j-1]=1;
                if(lim(i+1,j+1) && board[i+1][j+1]=='.' )
                    black_check_board[i+1][j+1]=1;
            }
            if(board[i][j]=='r')
            {
                set_rook(i,j,0);
            }
            if(board[i][j]=='b')
            {
                set_bishop(i,j,0);
            }
            if(board[i][j]=='n')
            {
                set_knight(i,j,0);
            }
            if(board[i][j]=='q')
            {
                set_rook(i,j,0);
                set_bishop(i,j,0);
            }
        }
}



bool bishop_check(int i,int j,bool flag)
{
    int x,y;
    x=i,y=j;
    char ch='Q';
    if(flag)ch='q';

    while( lim(x,y) )
    {
        x++,y++;
        if(board[x][y]!='.')
        {
            if(board[x][y]==ch) return true;
            else break;
        }
    }
    x=i,y=j;

    while( lim(x,y) )
    {
        x--,y--;
        if(board[x][y]!='.')
        {
            if(board[x][y]==ch)return true;
            else break;
        }
    }

    x=i,y=j;
    while( lim(x,y) )
    {
        x++,y--;
        if(board[x][y]!='.')
        {
            if(board[x][y]==ch)return true;
            else break;
        }
    }

    x=i,y=j;
    while( lim(x,y) )
    {
        x--,y++;
        if(board[x][y]!='.')
        {
            if(board[x][y]==ch)return true;
            else break;
        }
    }
    return false;
}

bool rook_check(int i,int j,bool flag)
{
    int x=i,y=j;
    char ch='Q';
    if(flag)ch='q';

    while( lim(x,y) )
    {
        x++;
        if(board[x][y]!=ch)return true;
        else break;
    }
    x=i,y=j;
    while( lim(x,y) )
    {
        x--;
        if(board[x][y]!=ch)return true;
        else break;
    }
    x=i,y=j;
    while( lim(x,y) )
    {
        y++;
        if(board[x][y]!=ch)return true;
        else break;
    }
    x=i,y=j;
    while( lim(x,y) )
    {
        y--;
        if(board[x][y]!=ch)return true;
        else break;
    }
    return false;
}


void board_fill()
{
    fo(i,0,10)
    {
        fo(j,0,10)board[i][j]='.';
    }
}
void board_print()
{
    for(int i=4; i>=1; i--)
    {
        for(int j=1; j<=4; j++)cout<<board[i][j];
        cout<<endl;
    }
}


vector< node > v;
node a;

void ROOK_TAKE(int i,int j,int flag)
{
    if(flag)
    {

        int x=i,y=j;
        while(true)
        {
            x++;
            if(!lim(x,y))break;
            if( board[x][y] !='.' && LOWER(board[x][y]) && black_check_board[x][y]==0 )
            {
                a.from_x=i,a.from_y=j,a.to_x=x,a.to_y=y,a.p=value(board[x][y]),a.ch='R';
                v.pb(a);
                break;
            }
        }
        x=i,y=j;
        while(true)
        {
            x--;
            if(!lim(x,y))break;
            if( board[x][y] !='.' && LOWER(board[x][y]) && black_check_board[x][y]==0 )
            {
                a.from_x=i,a.from_y=j,a.to_x=x,a.to_y=y,a.p=value(board[x][y]),a.ch='R';
                v.pb(a);
                break;
            }
        }
        x=i,y=j;
        while(true)
        {
            y++;
            if(!lim(x,y))break;
            if( board[x][y] !='.' && LOWER(board[x][y]) && black_check_board[x][y]==0 )
            {
                a.from_x=i,a.from_y=j,a.to_x=x,a.to_y=y,a.p=value(board[x][y]),a.ch='R';
                v.pb(a);
                break;
            }
        }
        x=i,y=j;
        while(true)
        {
            y--;
            if(!lim(x,y))break;
            if( board[x][y] !='.' && LOWER(board[x][y]) && black_check_board[x][y]==0 )
            {
                a.from_x=i,a.from_y=j,a.to_x=x,a.to_y=y,a.p=value(board[x][y]),a.ch='R';
                v.pb(a);
                break;
            }
        }
    }
    else
    {
        int x=i,y=j;
        while(true)
        {
            x++;
            if(!lim(x,y))break;
            if( board[x][y] !='.' && !LOWER(board[x][y]) && white_check_board[x][y]==0 )
            {
                a.from_x=i,a.from_y=j,a.to_x=x,a.to_y=y,a.p=value(board[x][y]),a.ch='r';
                v.pb(a);
                break;
            }
        }
        x=i,y=j;
        while(true)
        {
            x--;
            if(!lim(x,y))break;
            if( board[x][y] !='.' && !LOWER(board[x][y]) && white_check_board[x][y]==0 )
            {
                a.from_x=i,a.from_y=j,a.to_x=x,a.to_y=y,a.p=value(board[x][y]),a.ch='r';
                v.pb(a);
                break;
            }
        }
        x=i,y=j;
        while(true)
        {
            y++;
            if(!lim(x,y))break;
            if( board[x][y] !='.' && !LOWER(board[x][y]) && white_check_board[x][y]==0 )
            {
                a.from_x=i,a.from_y=j,a.to_x=x,a.to_y=y,a.p=value(board[x][y]),a.ch='r';
                v.pb(a);
                break;
            }
        }
        x=i,y=j;
        while(true)
        {
            y--;
            if(!lim(x,y))break;
            if( board[x][y] !='.' && !LOWER(board[x][y]) && white_check_board[x][y]==0 )
            {
                a.from_x=i,a.from_y=j,a.to_x=x,a.to_y=y,a.p=value(board[x][y]),a.ch='r';
                v.pb(a);
                break;
            }
        }

    }
}

void B_TAKE(int i,int j,int flag)
{
    if(flag)
    {

        int x=i,y=j;
        while(true)
        {
            x++;
            y++;
            if(!lim(x,y))break;
            if( board[x][y] !='.' && LOWER(board[x][y]) && black_check_board[x][y]==0 )
            {
                a.from_x=i,a.from_y=j,a.to_x=x,a.to_y=y,a.p=value(board[x][y]),a.ch='B';
                v.pb(a);
                break;
            }
        }
        x=i,y=j;
        while(true)
        {
            x--;
            y--;
            if(!lim(x,y))break;
            if( board[x][y] !='.' && LOWER(board[x][y]) && black_check_board[x][y]==0 )
            {
                a.from_x=i,a.from_y=j,a.to_x=x,a.to_y=y,a.p=value(board[x][y]),a.ch='B';
                v.pb(a);
                break;
            }
        }
        x=i,y=j;
        while(true)
        {
            x++,y--;
            if(!lim(x,y))break;
            if( board[x][y] !='.' && LOWER(board[x][y]) && black_check_board[x][y]==0 )
            {
                a.from_x=i,a.from_y=j,a.to_x=x,a.to_y=y,a.p=value(board[x][y]),a.ch='B';
                v.pb(a);
                break;
            }
        }
        x=i,y=j;
        while(true)
        {
            x--,y++;
            if(!lim(x,y))break;
            if( board[x][y] !='.' && LOWER(board[x][y]) && black_check_board[x][y]==0 )
            {
                a.from_x=i,a.from_y=j,a.to_x=x,a.to_y=y,a.p=value(board[x][y]),a.ch='B';
                v.pb(a);
                break;
            }
        }
    }
    else
    {
        int x=i,y=j;
        while(true)
        {
            x++;
            y++;
            if(!lim(x,y))break;
            if( board[x][y] !='.' && !LOWER(board[x][y]) && white_check_board[x][y]==0 )
            {
                a.from_x=i,a.from_y=j,a.to_x=x,a.to_y=y,a.p=value(board[x][y]),a.ch='b';
                v.pb(a);
                break;
            }
        }
        x=i,y=j;
        while(true)
        {
            x--;
            y--;
            if(!lim(x,y))break;
            if( board[x][y] !='.' && !LOWER(board[x][y]) && white_check_board[x][y]==0 )
            {
                a.from_x=i,a.from_y=j,a.to_x=x,a.to_y=y,a.p=value(board[x][y]),a.ch='b';
                v.pb(a);
                break;
            }
        }
        x=i,y=j;
        while(true)
        {
            x++,y--;
            if(!lim(x,y))break;
            if( board[x][y] !='.' && !LOWER(board[x][y]) && white_check_board[x][y]==0 )
            {
                a.from_x=i,a.from_y=j,a.to_x=x,a.to_y=y,a.p=value(board[x][y]),a.ch='b';
                v.pb(a);
                break;
            }
        }
        x=i,y=j;
        while(true)
        {
            x--,y++;
            if(!lim(x,y))break;
            if( board[x][y] !='.' && !LOWER(board[x][y]) && white_check_board[x][y]==0 )
            {
                a.from_x=i,a.from_y=j,a.to_x=x,a.to_y=y,a.p=value(board[x][y]),a.ch='b';
                v.pb(a);
                break;
            }
        }

    }
}

void K_TAKE(int i,int j,int flag)
{
    if(flag)
    {
        for(int inc=0; inc<8; inc++)
        {
            int x,y;
            x=i+kx[inc] ,y=j+ky[inc];
            if(lim(x,y) && board[x][y]!='.' && LOWER(board[x][y]) && black_check_board[x][y]==0 )
            {
                a.from_x=i,a.from_y=j,a.to_x=x,a.to_y=y,a.p=value(board[x][y]),a.ch='N';
                v.pb(a);
                break;
            }
        }
    }
    else
    {
        for(int inc=0; inc<8; inc++)
        {
            int x,y;
            x=i+kx[inc] ,y=j+ky[inc];
            if(lim(x,y) && board[x][y]!='.' && !LOWER(board[x][y]) && white_check_board[x][y]==0 )
            {
                a.from_x=i,a.from_y=j,a.to_x=x,a.to_y=y,a.p=value(board[x][y]),a.ch='n';
                v.pb(a);
                break;
            }
        }
    }
}

void Q_TAKE(int i,int j,int flag)
{
    //st
    if(flag)
    {

        int x=i,y=j;
        while(true)
        {
            x++;
            if(!lim(x,y))break;
            if( board[x][y] !='.' && LOWER(board[x][y]) && black_check_board[x][y]==0 )
            {
                a.from_x=i,a.from_y=j,a.to_x=x,a.to_y=y,a.p=value(board[x][y]),a.ch='Q';
                v.pb(a);
                break;
            }
        }
        x=i,y=j;
        while(true)
        {
            x--;
            if(!lim(x,y))break;
            if( board[x][y] !='.' && LOWER(board[x][y]) && black_check_board[x][y]==0 )
            {
                a.from_x=i,a.from_y=j,a.to_x=x,a.to_y=y,a.p=value(board[x][y]),a.ch='Q';
                v.pb(a);
                break;
            }
        }
        x=i,y=j;
        while(true)
        {
            y++;
            if(!lim(x,y))break;
            if( board[x][y] !='.' && LOWER(board[x][y]) && black_check_board[x][y]==0 )
            {
                a.from_x=i,a.from_y=j,a.to_x=x,a.to_y=y,a.p=value(board[x][y]),a.ch='Q';
                v.pb(a);
                break;
            }
        }
        x=i,y=j;
        while(true)
        {
            y--;
            if(!lim(x,y))break;
            if( board[x][y] !='.' && LOWER(board[x][y]) && black_check_board[x][y]==0 )
            {
                a.from_x=i,a.from_y=j,a.to_x=x,a.to_y=y,a.p=value(board[x][y]),a.ch='Q';
                v.pb(a);
                break;
            }
        }
    }
    else
    {
        int x=i,y=j;
        while(true)
        {
            x++;
            if(!lim(x,y))break;
            if( board[x][y] !='.' && !LOWER(board[x][y]) && white_check_board[x][y]==0 )
            {
                a.from_x=i,a.from_y=j,a.to_x=x,a.to_y=y,a.p=value(board[x][y]),a.ch='q';
                v.pb(a);
                break;
            }
        }
        x=i,y=j;
        while(true)
        {
            x--;
            if(!lim(x,y))break;
            if( board[x][y] !='.' && !LOWER(board[x][y]) && white_check_board[x][y]==0 )
            {
                a.from_x=i,a.from_y=j,a.to_x=x,a.to_y=y,a.p=value(board[x][y]),a.ch='q';
                v.pb(a);
                break;
            }
        }
        x=i,y=j;
        while(true)
        {
            y++;
            if(!lim(x,y))break;
            if( board[x][y] !='.' && !LOWER(board[x][y]) && white_check_board[x][y]==0 )
            {
                a.from_x=i,a.from_y=j,a.to_x=x,a.to_y=y,a.p=value(board[x][y]),a.ch='q';
                v.pb(a);
                break;
            }
        }
        x=i,y=j;
        while(true)
        {
            y--;
            if(!lim(x,y))break;
            if( board[x][y] !='.' && !LOWER(board[x][y]) && white_check_board[x][y]==0 )
            {
                a.from_x=i,a.from_y=j,a.to_x=x,a.to_y=y,a.p=value(board[x][y]),a.ch='q';
                v.pb(a);
                break;
            }
        }

    }

    if(flag)
    {

        int x=i,y=j;
        while(true)
        {
            x++;
            y++;
            if(!lim(x,y))break;
            if( board[x][y] !='.' && LOWER(board[x][y]) && black_check_board[x][y]==0 )
            {
                a.from_x=i,a.from_y=j,a.to_x=x,a.to_y=y,a.p=value(board[x][y]),a.ch='Q';
                v.pb(a);
                break;
            }
        }
        x=i,y=j;
        while(true)
        {
            x--;
            y--;
            if(!lim(x,y))break;
            if( board[x][y] !='.' && LOWER(board[x][y]) && black_check_board[x][y]==0 )
            {
                a.from_x=i,a.from_y=j,a.to_x=x,a.to_y=y,a.p=value(board[x][y]),a.ch='Q';
                v.pb(a);
                break;
            }
        }
        x=i,y=j;
        while(true)
        {
            x++,y--;
            if(!lim(x,y))break;
            if( board[x][y] !='.' && LOWER(board[x][y]) && black_check_board[x][y]==0 )
            {
                a.from_x=i,a.from_y=j,a.to_x=x,a.to_y=y,a.p=value(board[x][y]),a.ch='Q';
                v.pb(a);
                break;
            }
        }
        x=i,y=j;
        while(true)
        {
            x--,y++;
            if(!lim(x,y))break;
            if( board[x][y] !='.' && LOWER(board[x][y]) && black_check_board[x][y]==0 )
            {
                a.from_x=i,a.from_y=j,a.to_x=x,a.to_y=y,a.p=value(board[x][y]),a.ch='Q';
                v.pb(a);
                break;
            }
        }
    }
    else
    {
        int x=i,y=j;
        while(true)
        {
            x++;
            y++;
            if(!lim(x,y))break;
            if( board[x][y] !='.' && !LOWER(board[x][y]) && white_check_board[x][y]==0 )
            {
                a.from_x=i,a.from_y=j,a.to_x=x,a.to_y=y,a.p=value(board[x][y]),a.ch='q';
                v.pb(a);
                break;
            }
        }
        x=i,y=j;
        while(true)
        {
            x--;
            y--;
            if(!lim(x,y))break;
            if( board[x][y] !='.' && !LOWER(board[x][y]) && white_check_board[x][y]==0 )
            {
                a.from_x=i,a.from_y=j,a.to_x=x,a.to_y=y,a.p=value(board[x][y]),a.ch='q';
                v.pb(a);
                break;
            }
        }
        x=i,y=j;
        while(true)
        {
            x++,y--;
            if(!lim(x,y))break;
            if( board[x][y] !='.' && !LOWER(board[x][y]) && white_check_board[x][y]==0 )
            {
                a.from_x=i,a.from_y=j,a.to_x=x,a.to_y=y,a.p=value(board[x][y]),a.ch='q';
                v.pb(a);
                break;
            }
        }
        x=i,y=j;
        while(true)
        {
            x--,y++;
            if(!lim(x,y))break;
            if( board[x][y] !='.' && !LOWER(board[x][y]) && white_check_board[x][y]==0 )
            {
                a.from_x=i,a.from_y=j,a.to_x=x,a.to_y=y,a.p=value(board[x][y]),a.ch='q';
                v.pb(a);
                break;
            }
        }

    }
    //ed
}




int rmx[]= {1,-1,0,0};
int rmy[]= {0,0,1,-1};

int bmx[]= {1,-1,1,-1};
int bmy[]= {1,-1,-1,1};


//black move
bool black_move()
{
    //winning move
    for(int i=1; i<5; i++)
    {
        for(int j=1; j<5; j++)
        {

            if(board[i][j]=='p') //black pawn
            {
                if(board[i-1][j+1]=='Q' || board[i-1][j-1]=='Q')
                    return true;
            }

            if(board[i][j]=='n') //Black knight
            {
                fo(v,0,8)
                {
                    if(board[i+kx[v]][j+ky[v]] =='Q' )
                        return true;
                }
            }

            if(board[i][j]='b')  //black bishop
            {
                if(bishop_check(i,j,0))
                {
                    return true;
                }
            }

            if(board[i][j]=='r')  // black rook
            {
                if(rook_check(i,j,0))
                {
                    return true;
                }
            }

            if(board[i][j]=='q')   //white Queen
            {
                if(rook_check(i,j,0) || bishop_check(i,j,0))
                {
                    return true;
                }
            }
        }
    }
    //end of winning move

    v.clear();

    fo(i,1,5)
    {
        fo(j,1,5)
        {
            if(board[i][j]=='p')
            {
                if(lim(i-1,j-1))
                {
                    if(board[i-1][j-1] !='.' && !LOWER(board[i-1][j-1]) && white_check_board[i-1][j-1]==0 )
                    {
                        a.from_x=i;
                        a.from_y=j;
                        a.to_x=i-1;
                        a.to_y=j-1;
                        a.ch='p';
                        a.p=value(board[i-1][j-1]);
                        v.pb(a);
                    }
                    if(lim(i-1,j+1))
                    {
                        if(board[i-1][j+1] !='.' && !LOWER(board[i-1][j+1]) && white_check_board[i-1][j+1]==0 )
                        {
                            a.from_x=i;
                            a.from_y=j;
                            a.to_x=i-1;
                            a.to_y=j+1;
                            a.p=value(board[i-1][j+1]);
                            a.ch='P';
                            v.pb(a);
                        }
                    }
                }
            }

            if(board[i][j]=='r')
            {
                ROOK_TAKE(i,j,0);
            }
            if(board[i][j]=='b')
            {
                B_TAKE(i,j,0);
            }
            if(board[i][j]=='n')
            {
                K_TAKE(i,j,0);
            }
            if(board[i][j]=='q')
            {
                Q_TAKE(i,j,0);
            }
        }
    }

    if(v.empty())
    {

        vector< pair< int, pair<int,int> > > vv;
        vector< pair< int, pair<int,int> > > pices;
        fo(i,1,5)
        {
            fo(j,1,5)
            {
                if(board[i][j]!='.' && LOWER(board[i][j] && white_check_board[i][j] )  )
                {
                    vv.pb( MP(value(board[i][j]) , MP(i,j) )  ) ;
                }
                if(board[i][j]!='.' && LOWER(board[i][j]) )
                {
                    pices.pb( MP(value(board[i][j]), MP(i,j) ) );
                }
            }
        }
        if(vv.empty())
        {
            sort(pices.begin(),pices.end());
            bool ok=false;
            int check[10];
            memset(check,0,sizeof check);

            fo(i,0,sz(pices))
            {
                int _x=pices[i].yy.xx,_y=pices[i].yy.yy;

                if(board[_x][_y]=='p')
                {
                    if( lim(_x-1,_y-1) && board[_x-1][_y-1]=='.' )
                    {
                        board[_x-1][_y-1]='p';
                        board[_x][_y]='.';
                        if(_x-1 == 0) board[_x-1][_y-1]='r';
                        ok=true;
                        break;
                    }
                    if(lim(_x-1,_y+1) && board[_x-1][_y+1]=='.' )
                    {
                        board[_x-1][_y+1]='p';
                        board[_x][_y]='.';
                        if(_x-1==0) board[ _x-1 ][ _y+1 ]='r';
                        ok=true;
                        break;
                    }
                    if(lim(_x-1,_y) && board[_x-1][_y]=='.' && _x-1==0 )
                    {
                        board[_x-1][_y]='r';
                        board[_x][_y]='.';
                        ok=true;
                        break;
                    }
                }

                if(board[_x][_y]=='r')
                {
                    memset(check,0,sizeof check);
                    fo(low,0,4)
                    {
                        int u=_x+rmx[low],v=_y+rmy[low];
                        if(lim(u,v) && !check[low] && board[u][v]=='.' )
                        {
                            board[u][v]='r';
                            board[_x][_y]='.';
                            ok=true;
                            break;
                        }
                        else
                        {
                            check[low]=1;
                        }
                    }
                }

                if(board[_x][_y]=='b')
                {
                    memset(check,0,sizeof check);
                    fo(low,0,4)
                    {
                        int u=_x+bmx[low],v=_y+bmy[low];
                        if(lim(u,v) && !check[low] && board[u][v]=='.' )
                        {
                            board[u][v]='b';
                            board[_x][_y]='.';
                            ok=true;
                            break;
                        }
                        else
                        {
                            check[low]=1;
                        }
                    }
                }

                if(board[_x][_y]=='n')
                {
                    fo(low,0,8)
                    {
                        int u=_x+kx[low],v=_y+ky[low];
                        if(lim(u,v) && board[u][v]=='.')
                        {
                            board[u][v]='n';
                            board[_x][_y]='.';
                            ok=true;
                            break;
                        }
                    }
                }
                if(board[_x][_y]=='q')
                {
                    memset(check,0,sizeof check);
                    fo(low,0,4)
                    {
                        int u=_x+bmx[low],v=_y+bmy[low];
                        if(lim(u,v) && !check[low] && board[u][v]=='.' )
                        {
                            board[u][v]='q';
                            board[_x][_y]='.';
                            ok=true;
                            break;
                        }
                        else
                        {
                            check[low]=1;
                        }
                    }

                    memset(check,0,sizeof check);
                    fo(low,0,4)
                    {
                        int u=_x+rmx[low],v=_y+rmy[low];
                        if(lim(u,v) && !check[low] && board[u][v]=='.' )
                        {
                            board[u][v]='q';
                            board[_x][_y]='.';
                            ok=true;
                            break;
                        }
                        else
                        {
                            check[low]=1;
                        }
                    }


                }

            }

        }
        else
        {

            //st
            //sort(pices.begin(),pices.end());
            sort(vv.begin(),vv.end());
            bool ok=false;
            int check[10];
            memset(check,0,sizeof check);

            for(int i=sz(vv)-1;i>=0;i--)
            {
                int _x=vv[i].yy.xx,_y=vv[i].yy.yy;

                if(board[_x][_y]=='p')
                {
                    if( lim(_x-1,_y-1) && board[_x-1][_y-1]=='.' )
                    {
                        board[_x-1][_y-1]='p';
                        board[_x][_y]='.';
                        if(_x-1 == 0) board[_x-1][_y-1]='r';
                        ok=true;
                        break;
                    }
                    if(lim(_x-1,_y+1) && board[_x-1][_y+1]=='.' )
                    {
                        board[_x-1][_y+1]='p';
                        board[_x][_y]='.';
                        if(_x-1==0) board[ _x-1 ][ _y+1 ]='r';
                        ok=true;
                        break;
                    }
                    if(lim(_x-1,_y) && board[_x-1][_y]=='.' && _x-1==0 )
                    {
                        board[_x-1][_y]='r';
                        board[_x][_y]='.';
                        ok=true;
                        break;
                    }
                }

                if(board[_x][_y]=='r')
                {
                    memset(check,0,sizeof check);
                    fo(low,0,4)
                    {
                        int u=_x+rmx[low],v=_y+rmy[low];
                        if(lim(u,v) && !check[low] && board[u][v]=='.' )
                        {
                            board[u][v]='r';
                            board[_x][_y]='.';
                            ok=true;
                            break;
                        }
                        else
                        {
                            check[low]=1;
                        }
                    }
                }

                if(board[_x][_y]=='b')
                {
                    memset(check,0,sizeof check);
                    fo(low,0,4)
                    {
                        int u=_x+bmx[low],v=_y+bmy[low];
                        if(lim(u,v) && !check[low] && board[u][v]=='.' )
                        {
                            board[u][v]='b';
                            board[_x][_y]='.';
                            ok=true;
                            break;
                        }
                        else
                        {
                            check[low]=1;
                        }
                    }
                }

                if(board[_x][_y]=='n')
                {
                    fo(low,0,8)
                    {
                        int u=_x+kx[low],v=_y+ky[low];
                        if(lim(u,v) && board[u][v]=='.')
                        {
                            board[u][v]='n';
                            board[_x][_y]='.';
                            ok=true;
                            break;
                        }
                    }
                }
                if(board[_x][_y]=='q')
                {
                    memset(check,0,sizeof check);
                    fo(low,0,4)
                    {
                        int u=_x+bmx[low],v=_y+bmy[low];
                        if(lim(u,v) && !check[low] && board[u][v]=='.' )
                        {
                            board[u][v]='q';
                            board[_x][_y]='.';
                            ok=true;
                            break;
                        }
                        else
                        {
                            check[low]=1;
                        }
                    }

                    memset(check,0,sizeof check);
                    fo(low,0,4)
                    {
                        int u=_x+rmx[low],v=_y+rmy[low];
                        if(lim(u,v) && !check[low] && board[u][v]=='.' )
                        {
                            board[u][v]='q';
                            board[_x][_y]='.';
                            ok=true;
                            break;
                        }
                        else
                        {
                            check[low]=1;
                        }
                    }


                }

            }
            //ed
        }

    }
    else
    {
        int mx=-1; // highest value take..
        int sx,sy;
        int tx,ty;
        char cc;
        for( auto x: v)
        {
            if(x.p >mx )
            {
                mx=x.p;
                sx=x.to_x,sy=x.to_y;
                tx=x.from_x,ty=x.from_y;
                cc=x.ch;
            }
        }
        board[tx][ty]='.';
        board[sx][sy]=cc;
    }

    return false;
}
//end black move




bool white_move()
{
    //winning move
    for(int i=1; i<5; i++)
    {
        for(int j=1; j<5; j++)
        {

            if(board[i][j]=='P') //white pawn
            {
                if(board[i+1][j+1]=='q' || board[i+1][j-1]=='q')
                    return true;
            }

            if(board[i][j]=='N') //white knight
            {
                fo(v,0,8)
                {
                    if(board[i+kx[v]][j+ky[v]] =='q' )
                        return true;
                }
            }

            if(board[i][j]='B')  //white bishop
            {
                if(bishop_check(i,j,1))
                {
                    return true;
                }
            }

            if(board[i][j]=='R')  // white rook
            {
                if(rook_check(i,j,1))
                {
                    return true;
                }
            }

            if(board[i][j]=='Q')   //white Queen
            {
                if(rook_check(i,j,1) || bishop_check(i,j,1))
                {
                    return true;
                }
            }
        }
    }
    //end of winning move

    v.clear();

    fo(i,1,5)
    {
        fo(j,1,5)
        {
            if(board[i][j]=='P')
            {
                if(lim(i+1,j-1))
                {
                    if(board[i+1][j-1] !='.' && LOWER(board[i+1][j-1]) && black_check_board[i+1][j-1]==0 )
                    {
                        a.from_x=i;
                        a.from_y=j;
                        a.to_x=i+1;
                        a.to_y=j-1;
                        a.ch='P';
                        a.p=value(board[i+1][j-1]);
                        v.pb(a);
                    }
                    if(lim(i+1,j+1))
                    {
                        if(board[i+1][j+1] !='.' && LOWER(board[i+1][j+1]) && black_check_board[i+1][j+1]==0 )
                        {
                            a.from_x=i;
                            a.from_y=j;
                            a.to_x=i+1;
                            a.to_y=j+1;
                            a.p=value(board[i+1][j+1]);
                            a.ch='P';
                            v.pb(a);
                        }
                    }
                }
            }

            if(board[i][j]=='R')
            {
                ROOK_TAKE(i,j,1);
            }
            if(board[i][j]=='B')
            {
                B_TAKE(i,j,1);
            }
            if(board[i][j]=='N')
            {
                K_TAKE(i,j,1);
            }
            if(board[i][j]=='Q')
            {
                Q_TAKE(i,j,1);
            }
        }
    }

    if(v.empty())
    {

        vector< pair< int, pair<int,int> > > vv;
        vector< pair< int, pair<int,int> > > pices;
        fo(i,1,5)
        {
            fo(j,1,5)
            {
                if(board[i][j]!='.' && !LOWER(board[i][j] && black_check_board[i][j] )  )
                {
                    vv.pb( MP(value(board[i][j]) , MP(i,j) )  ) ;
                }
                if(board[i][j]!='.' && !LOWER(board[i][j]) )
                {
                    pices.pb( MP(value(board[i][j]), MP(i,j) ) );
                }
            }
        }
        if(vv.empty())
        {
            sort(pices.begin(),pices.end());
            bool ok=false;
            int check[10];
            memset(check,0,sizeof check);

            fo(i,0,sz(pices))
            {
                int _x=pices[i].yy.xx,_y=pices[i].yy.yy;

                if(board[_x][_y]=='P')
                {
                    if( lim(_x+1,_y-1) && board[_x+1][_y-1]=='.' )
                    {
                        board[_x+1][_y-1]='P';
                        board[_x][_y]='.';
                        if(_x+1 == 4) board[_x+1][_y-1]='R';
                        ok=true;
                        break;
                    }
                    if(lim(_x+1,_y+1) && board[_x+1][_y+1]=='.' )
                    {
                        board[_x+1][_y+1]='P';
                        board[_x][_y]='.';
                        if(_x+1==4) board[ _x+1 ][ _y+1 ]='R';
                        ok=true;
                        break;
                    }
                    if(lim(_x+1,_y) && board[_x+1][_y]=='.' && _x+1==4 )
                    {
                        board[_x+1][_y]='R';
                        board[_x][_y]='.';
                        ok=true;
                        break;
                    }
                }

                if(board[_x][_y]=='R')
                {
                    memset(check,0,sizeof check);
                    fo(low,0,4)
                    {
                        int u=_x+rmx[low],v=_y+rmy[low];
                        if(lim(u,v) && !check[low] && board[u][v]=='.' )
                        {
                            board[u][v]='R';
                            board[_x][_y]='.';
                            ok=true;
                            break;
                        }
                        else
                        {
                            check[low]=1;
                        }
                    }
                }

                if(board[_x][_y]=='B')
                {
                    memset(check,0,sizeof check);
                    fo(low,0,4)
                    {
                        int u=_x+bmx[low],v=_y+bmy[low];
                        if(lim(u,v) && !check[low] && board[u][v]=='.' )
                        {
                            board[u][v]='B';
                            board[_x][_y]='.';
                            ok=true;
                            break;
                        }
                        else
                        {
                            check[low]=1;
                        }
                    }
                }

                if(board[_x][_y]=='N')
                {
                    fo(low,0,8)
                    {
                        int u=_x+kx[low],v=_y+ky[low];
                        if(lim(u,v) && board[u][v]=='.')
                        {
                            board[u][v]='N';
                            board[_x][_y]='.';
                            ok=true;
                            break;
                        }
                    }
                }
                if(board[_x][_y]=='Q')
                {
                    memset(check,0,sizeof check);
                    fo(low,0,4)
                    {
                        int u=_x+bmx[low],v=_y+bmy[low];
                        if(lim(u,v) && !check[low] && board[u][v]=='.' )
                        {
                            board[u][v]='Q';
                            board[_x][_y]='.';
                            ok=true;
                            break;
                        }
                        else
                        {
                            check[low]=1;
                        }
                    }

                    memset(check,0,sizeof check);
                    fo(low,0,4)
                    {
                        int u=_x+rmx[low],v=_y+rmy[low];
                        if(lim(u,v) && !check[low] && board[u][v]=='.' )
                        {
                            board[u][v]='Q';
                            board[_x][_y]='.';
                            ok=true;
                            break;
                        }
                        else
                        {
                            check[low]=1;
                        }
                    }


                }

            }

        }
        else
        {

            //st
            //sort(pices.begin(),pices.end());
            sort(vv.begin(),vv.end());
            bool ok=false;
            int check[10];
            memset(check,0,sizeof check);

            for(int i=sz(vv)-1; i>=0; i--)
            {
                int _x=vv[i].yy.xx,_y=vv[i].yy.yy;

                if(board[_x][_y]=='P')
                {
                    if( lim(_x+1,_y-1) && board[_x+1][_y-1]=='.' )
                    {
                        board[_x+1][_y-1]='P';
                        board[_x][_y]='.';
                        if(_x+1 == 4) board[_x+1][_y-1]='R';
                        ok=true;
                        break;
                    }
                    if(lim(_x+1,_y+1) && board[_x+1][_y+1]=='.' )
                    {
                        board[_x+1][_y+1]='P';
                        board[_x][_y]='.';
                        if(_x+1==4) board[ _x+1 ][ _y+1 ]='R';
                        ok=true;
                        break;
                    }
                    if(lim(_x+1,_y) && board[_x+1][_y]=='.' && _x+1==4 )
                    {
                        board[_x+1][_y]='R';
                        board[_x][_y]='.';
                        ok=true;
                        break;
                    }
                }

                if(board[_x][_y]=='R')
                {
                    memset(check,0,sizeof check);
                    fo(low,0,4)
                    {
                        int u=_x+rmx[low],v=_y+rmy[low];
                        if(lim(u,v) && !check[low] && board[u][v]=='.' )
                        {
                            board[u][v]='R';
                            board[_x][_y]='.';
                            ok=true;
                            break;
                        }
                        else
                        {
                            check[low]=1;
                        }
                    }
                }

                if(board[_x][_y]=='B')
                {
                    memset(check,0,sizeof check);
                    fo(low,0,4)
                    {
                        int u=_x+bmx[low],v=_y+bmy[low];
                        if(lim(u,v) && !check[low] && board[u][v]=='.' )
                        {
                            board[u][v]='B';
                            board[_x][_y]='.';
                            ok=true;
                            break;
                        }
                        else
                        {
                            check[low]=1;
                        }
                    }
                }

                if(board[_x][_y]=='N')
                {
                    fo(low,0,8)
                    {
                        int u=_x+kx[low],v=_y+ky[low];
                        if(lim(u,v) && board[u][v]=='.')
                        {
                            board[u][v]='N';
                            board[_x][_y]='.';
                            ok=true;
                            break;
                        }
                    }
                }
                if(board[_x][_y]=='Q')
                {
                    memset(check,0,sizeof check);
                    fo(low,0,4)
                    {
                        int u=_x+bmx[low],v=_y+bmy[low];
                        if(lim(u,v) && !check[low] && board[u][v]=='.' )
                        {
                            board[u][v]='Q';
                            board[_x][_y]='.';
                            ok=true;
                            break;
                        }
                        else
                        {
                            check[low]=1;
                        }
                    }

                    memset(check,0,sizeof check);
                    fo(low,0,4)
                    {
                        int u=_x+rmx[low],v=_y+rmy[low];
                        if(lim(u,v) && !check[low] && board[u][v]=='.' )
                        {
                            board[u][v]='Q';
                            board[_x][_y]='.';
                            ok=true;
                            break;
                        }
                        else
                        {
                            check[low]=1;
                        }
                    }


                }

            }
            //ed
        }

    }
    else
    {
        int mx=-1; // highest value take..
        int sx,sy;
        int tx,ty;
        char cc;
        for( auto x: v)
        {
            if(x.p >mx )
            {
                mx=x.p;
                sx=x.to_x,sy=x.to_y;
                tx=x.from_x,ty=x.from_y;
                cc=x.ch;
            }
        }
        board[tx][ty]='.';
        board[sx][sy]=cc;
    }

    return false;
}


int main()
{
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    int t;
    char cols,name;
    cin>>t;

    while(t--)
    {
        board_fill();
        int white_cnt,black_cnt,moves;
        int x,y;
        cin>>white_cnt>>black_cnt>>moves;

        fo(i,0,white_cnt)
        {
            cin>>name>>cols>>x;
            y=(cols-'A')+1;
            board[x][y]=name;
        }
        fo(i,0,black_cnt)
        {
            cin>>name>>cols>>x;
            y=(cols-'A')+1;
            board[x][y]=tolower(name);
        }
        bool ans=false;
        bool as;

        for(int i=1; i<=moves; i++)
        {
            if(i&1)
            {
                set_black_check();
                set_white_check();
                as=white_move();
                if(as){
                    ans=true;
                    break;
                }
            }
            else
            {
                set_white_check();
                set_black_check();
                as=black_move();
                if(as){
                    ans=false;
                    break;
                }
            }
        }

        puts( ans ? "YES" : "NO" );




    }

    return  0;
}
















